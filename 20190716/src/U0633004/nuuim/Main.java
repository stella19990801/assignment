package U0633004.nuuim;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;
class BigNumber{
    private ArrayList<Integer> arr;
    public BigNumber(){
        arr = new ArrayList<Integer>();
    }
    public BigNumber(String s){
        arr = new ArrayList<Integer>();
        int a=0, j = s.length();
        for(int i = 0; i <  s.length(); i++){
           a = Integer.parseInt(s.substring(j-1,j));
           arr.add(i,a);
           j--;
       }
    }
    public void pri(BigNumber b){
        for(int i = (arr.size()-1); i >= 0;i--) {
            if(b.arr.get(arr.size()-1)==0&&i==arr.size()-1)continue;
            System.out.print(b.arr.get(i));
        }
        System.out.println();
    }
    public BigNumber A(BigNumber a, BigNumber b){
       BigNumber c = new BigNumber();
       int count=0;
       if(a.arr.size()<b.arr.size()){
           BigNumber tmp = new BigNumber();
           tmp = a;
           a = b;
           b = tmp;
       }
       //System.out.println(((a.arr.size()) - (b.arr.size())));
       for (int i = 0; i < (a.arr.size()); i++) {
           if (i < b.arr.size()) {
               if ((a.arr.get(i) + b.arr.get(i) + count) < 10) {
                   c.arr.add(a.arr.get(i) + b.arr.get(i) + count);
                   count = 0;
               } else {
                   c.arr.add((a.arr.get(i) + b.arr.get(i) + count) % 10);
                   count = (a.arr.get(i) + b.arr.get(i) + count) / 10;
               }
           } else {
               if ((a.arr.get(i) + count) < 10) {
                   c.arr.add(a.arr.get(i) + count);
                   count = 0;
               } else {
                   c.arr.add((a.arr.get(i) + count) % 10);
                   count = (a.arr.get(i) + count) / 10;
               }
           }
       }
//       pri(a);
//       pri(b);
       return c;
    }
    public BigNumber M (BigNumber a, BigNumber b){
        BigNumber c = new BigNumber();
        if(b.arr.size()>a.arr.size()){
            BigNumber tmp = new BigNumber();
            tmp = a;
            a = b;
            b = tmp;
        }
        int pre = 0;
        for(int i = 0; i < a.arr.size();i++){
            if(i < b.arr.size()){
                if((a.arr.get(i)-pre) >= b.arr.get(i)){
                    c.arr.add(a.arr.get(i)-b.arr.get(i)-pre);
                    pre = 0;
                }else{
                    c.arr.add(a.arr.get(i)-b.arr.get(i)-pre+10);
                    pre = 1;
                }
            }else{
                if((a.arr.get(i)-pre) >= 0){
                    c.arr.add(a.arr.get(i)-pre);
                    pre = 0;
                }else{
                    c.arr.add(a.arr.get(i)-pre+10);
                    pre = 1;
                }
            }
        }
//        pri(a);
//        pri(b);
        return c;
    }
}
///////////////////////////////////////////////////////////////////
//Stack -> ArrayList
class Stack{
    private ArrayList<String> arr;
    public Stack(){
        arr = new ArrayList<String>();
    }
    public  void push(String s){
        arr.add(s);
    }
    public void pop(){
        if(arr.size()>0) {
            System.out.println(arr.get(arr.size() - 1));
            arr.remove(arr.size() - 1);
        }else
            System.out.println("Is Empty!");
    }
    public void show(){
        for(int i = 0; i<arr.size();i++)
            System.out.println(arr.get(i));
    }
}

//Queue -> LinkList
class Queue{
    private LinkedList<String> arr;
    public Queue(){
        arr = new LinkedList<String>();
    }
    public void push(String s){
        arr.add(s);
    }public void pop(){
        if(arr.size()>0) {
            System.out.println(arr.get(0));
            arr.remove(0);
//            System.out.println(arr.get(arr.size() - 1));
//            arr.remove(arr.size()-1);
        }else
            System.out.println("Is Empty!");

    }
    public void show(){
        for(int i = 0; i<arr.size();i++)
            System.out.println(arr.get(i));
    }
}
///////////////////////////////////////////////////////////////////
class LAL{
    private HashMap<String,String> arr;
    public LAL(){
        arr = new HashMap<String,String>();
    }
    public void G(String s1, String s2){
        arr.put(s1,s2);
    }
    public void searchL(String s){
        String S[] = (arr.get(s)).split(",");
        System.out.println(S[0]+","+S[1]);
    }
    public void T(String s1, String s2){
        int l1 = 0,l2 = 0;
        String x,y;
        x = arr.get(s1);
        y = arr.get(s2);
        String S1[] = x.split(",");
        String S2[] = y.split(",");
        int X=Integer.parseInt(S1[2]), Y=Integer.parseInt(S2[2]),tmp=0;
        if(X<Y){
            tmp = X;
            X = Y;
            Y = tmp;
        }
        System.out.println("時差: "+(X-Y));
    }
}
public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        BigNumber x, y, z;
        String s1 ="", s2 = "";
        System.out.print("請輸入一數字:");
        s1 = sc.next();
        x = new BigNumber(s1);
        //x.pri(x);
        System.out.print("請輸入一想與之加減的數字:");
        s2 = sc.next();
        y = new BigNumber(s2);
        z = x.A(x,y);
        z.pri(z);
        z = x.M(x,y);
        z.pri(z);
///////////////////////////////////////////////////////////////////
        Stack s = new Stack();
        String a1,a2,a3;
        System.out.print("請輸入Stack:");
        a1 = sc.next();
        s.push(a1);
        System.out.print("請輸入Stack:");
        a2 = sc.next();
        s.push(a2);
        System.out.print("請輸入Stack:");
        a3 = sc.next();
        s.push(a3);
        s.show();
        s.pop();
        s.pop();
        s.pop();

        Queue q= new Queue();
        String b1,b2,b3;
        System.out.print("請輸入Queue:");
        b1 = sc.next();
        q.push(b1);
        System.out.print("請輸入Queue:");
        b2 = sc.next();
        q.push(b2);
        System.out.print("請輸入Queue:");
        b3 = sc.next();
        q.push(b3);
        q.show();
        q.pop();
        q.pop();
        q.pop();
///////////////////////////////////////////////////////////////////
        LAL L = new LAL();
        L.G("東京迪士尼","35°37N,139°52E,9");
        L.G("台北101","325°02N,121°33E,8");
        L.G("法國艾菲爾鐵塔","48°51N,2°17E,2");
        L.G("美國黃石國家公園","44°36N,110°30W,-7");
        System.out.print("請輸入想查詢哪2地時差:");
        String c1,c2,c3;
        c1 = sc.next();
        c2 = sc.next();
        L.T(c1,c2);
        System.out.print("請輸入想查詢和地之經緯度:");
        c3 = sc.next();
        L.searchL(c3);
    }
}
