package hht.im;

import java.util.Calendar;
import java.util.Scanner;

public class Main {
    static Thread C = new T3();
    static Thread D = new T3();
    static Thread E = new T4();
    static Thread F = new T4();
    static Thread A = new T1();
    static Thread B = new T2();
    public static class Test{
        static int n =0;
    }
    static class Count{
        int n;
    }
    static Count count = new Count();


    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int Ans = (int)(Math.random()*100+1);
        System.out.println("ANS :" + Ans);
        int a = 0;
        A.start();
        while(a != Ans){
            System.out.print("請輸入您所猜的數字 : ");
            a = sc.nextInt();
            if(a > Ans)
                System.out.println(">");
            else if(a < Ans)
                System.out.println("<");
            else
                System.out.println("=");
        }
        //A.stop();
        A.interrupt();
        B.start();
        String n = "";
        int N = 0;
        do{
            System.out.println("請輸入想要變動的秒數 : ");
            n = sc.next();
            if(!n.equals("stop")) {
                N = Integer.parseInt(n);
                ((T2)(B)).setS(N);
            }
        }while(!n.equals("stop"));
        B.interrupt();

//        C.start();
//        D.start();
        E.start();
        F.start();
    }
}
class T1 extends Thread{
    public void run(){
        int s = 0;
        for(int i = 0; i<Integer.MAX_VALUE; i++) {
            s++;
            //System.out.println(s);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("花費時間: " + s + "sec");
                return;
            }
        }

    }
}
class T2 extends Thread{
    private int S = 1;

    public void setS(int s) {
        S = s;
    }

    public void run(){
        for(int i = 0; i<Integer.MAX_VALUE; i++) {
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(System.currentTimeMillis());
            int hr = c.get(Calendar.HOUR_OF_DAY);
            int min = c.get(Calendar.MINUTE);
            int sec = c.get(Calendar.SECOND);
            System.out.println(hr + ":" + min + ":" + sec);
            try {
                Thread.sleep(1000 * S);
            } catch (InterruptedException e) {
                System.out.println("end");
                return;
            }
        }
    }
}
class T3 extends Thread{
    public void run(){
        for(int i = 0; i<600; i++) {
            Main.Test.n += 1;
            System.out.println(this.getName() + ":" + Main.Test.n);
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                System.out.println("end");
//                return;
//            }
        }
    }
}
class T4 extends Thread{
    public void run(){
        synchronized (Main.count) {
            for (int i = 0; i < 600; i++) {
                Main.count.n++;
                System.out.println(this.getName() + ":" + Main.count.n);
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    System.out.println("end");
//                    return;
//                }
            }
        }
    }
}