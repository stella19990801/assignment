package com.example.a20190730_listview_spanning_dialog_toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    ListView LV;
    Spinner SP;
    TextView TV;
    String [] data = {"1","2","3","4"};
    String [] d2 = {"a","b","c","d"};
    AlertDialog.Builder builder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LV = (ListView) findViewById(R.id.listview);
        ArrayAdapter ad = new ArrayAdapter(this, android.R.layout.simple_list_item_1, data);
        LV.setAdapter(ad);
        LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(MainActivity.this, "你選擇了" + data[i] + ", 位於第" + (i + 1) + "列", Toast.LENGTH_SHORT).show();
                builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("title");
                builder.setMessage("你選擇了" + data[i] + ", 位於第" + (i + 1) + "列");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        SP = (Spinner) findViewById(R.id.spinner);
        TV = (TextView)findViewById(R.id.textview);
        ArrayAdapter<String> ad2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, d2);
        SP.setAdapter(ad2);
        SP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TV.setText("Spinning : "+d2[i]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                TV.setText("");
            }
        });

    }
}
