package com.example.a20190730_3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    String [] data = {"陳宇佐", "温敏淦","馬麗菁","陳振東","張朝旭"};
    String [] job = {"助理教授", "副教授 兼 系主任","教授 兼 管理學院院長","教授","副教授 兼 圖書館系統管理組長"};
    String [] tel = {"聯絡電話:037-381513", "聯絡電話:037-381514","聯絡電話:037-381510","聯絡電話:037-381515","聯絡電話:037-381520"};
    int [] n = {0, 0, 0, 0, 0};
    int [] pic = {R.drawable.teacher12,R.drawable.teacher06,R.drawable.teacher04,R.drawable.teacher03,R.drawable.teacher05};
    ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setLogo(R.drawable.im1);
        lv = (ListView) findViewById(R.id.listview);
        BaseAdapter adapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return data.length;
            }

            @Override
            public Object getItem(int i) {
                return i;
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                View layout=View.inflate(MainActivity.this,R.layout.layout,null); //叫出剛才建立的自訂義layput
                ImageView img=(ImageView)layout.findViewById(R.id.img); //宣告圖片
                TextView textView=(TextView)layout.findViewById(R.id.textView); //宣告textview
                TextView j = (TextView)layout.findViewById(R.id.job);
                TextView t = (TextView)layout.findViewById(R.id.tel);
                img.setImageResource(pic[i]);
                textView.setText(data[i]);
                j.setText(job[i]);
                t.setText(tel[i]);
                return layout;  //return這個view
            }
        };
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                n[i]++;
                Toast.makeText(MainActivity.this,"點擊次數 : " + n[i],Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);     //產生選單並呼叫你的menu 作為選單樣式 
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        switch (menuItem.getItemId()) {
            case android.R.id.home: //對應到他的id 
                finish();  //當點擊這個按鈕就讓他退出
                return true;
        }
        int id = menuItem.getItemId();
        if(id == R.id.id1)
            Toast.makeText(MainActivity.this,"1",Toast.LENGTH_SHORT).show();
        else if(id == R.id.id2)
            Toast.makeText(MainActivity.this,"2",Toast.LENGTH_SHORT).show();
        if(id == R.id.id3)
            Toast.makeText(MainActivity.this,"☆",Toast.LENGTH_SHORT).show();
        else if(id == R.id.id4)
            Toast.makeText(MainActivity.this,"★",Toast.LENGTH_SHORT).show();
        return super.onOptionsItemSelected(menuItem);
    }

}
