package com.example.a20190730_1;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    ListView LV;
    Spinner SP;
    TextView TV;
    String [] data = {"1","2","3","4"};
    String [] d2 = {"a","b","c","d"};
    AlertDialog.Builder builder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.sun);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LV = (ListView) findViewById(R.id.listview);
        ArrayAdapter ad = new ArrayAdapter(this, android.R.layout.simple_list_item_1, data);
        LV.setAdapter(ad);
        LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(MainActivity.this, "你選擇了" + data[i] + ", 位於第" + (i + 1) + "列", Toast.LENGTH_SHORT).show();
            }
        });
        SP = (Spinner) findViewById(R.id.spinner);
        TV = (TextView)findViewById(R.id.textview);
        ArrayAdapter<String> ad2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, d2);
        SP.setAdapter(ad2);
        SP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TV.setText( d2[i]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                TV.setText("");
            }
        });
        builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("title");
//        builder.setMessage("hi");
        builder.setItems(data, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
//        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//
//            }
//        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);     //產生選單並呼叫你的menu 作為選單樣式 
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        switch (menuItem.getItemId()) {
            case android.R.id.home: //對應到他的id 
                 finish();  //當點擊這個按鈕就讓他退出
                 return true;
        }
        int id = menuItem.getItemId();
        if(id == R.id.id1)
            Toast.makeText(MainActivity.this,"set",Toast.LENGTH_SHORT).show();
        else if(id == R.id.id2)
            Toast.makeText(MainActivity.this,"help",Toast.LENGTH_SHORT).show();
        return super.onOptionsItemSelected(menuItem);
    }
}
