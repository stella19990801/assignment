<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title></title>
		<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
		</head>
	<body>
		<input type="text"/><br/>
		<span></span>
		<script type="text/javascript">
			$("input").keydown(function(e){
				$(this).css("background-color","#FFFFCC");
				$("span").html(e.keyCode+":"+e.key);
			});
			$("input").keyup(function(){
				$(this).css("background-color","white");
			});
		</script>
	</body>
</html>