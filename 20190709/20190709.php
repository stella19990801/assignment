<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title></title>
		<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
		</head>
	<body>
		<div id="over" style="background-color: #77DDFF;width:120px;height:20px;padding:10px">
			Mouse Over Me
		</div>
		<div id="click" style="background-color: #CCCCFF;width:120px;height:20px;padding:10px">
			Thank you
		</div>
		<script>
			$('#over').hover(function(){
				$(this).html("Thank You");//移進去
			},function(){
				$(this).html("Mouse Over Me");//移出來
			});
			$('#click').mousedown(function(){
				$(this).css("background-color","#FFDDAA");
				$(this).html("Release Me");
			});
			$('#click').mouseup(function(){
				$(this).css("background-color","#CCCCFF");
				$(this).html("Thank You");
			});
		</script>
	</body>
</html>