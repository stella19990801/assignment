<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title></title>
		<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	</head>
	<body>
		<button id="btn" name="button" type="button">取得時間</button>
		<span id="sp1"></span>
		<span id="sp2"></span>
		<script type="text/javascript">
			var d = new Date();
			var y = d.getFullYear();
			var m = d.getMonth()+1;
			var day = d.getDate();
			var h = d.getHours();
			var min = d.getMinutes();
			var s = d.getSeconds();
			$("body").keydown(function(e){
				if(e.keyCode==68){
					$('#sp1').html(y+"/"+m+"/"+day);
				}
			});
			$("#btn").click(function(){
				$('#sp2').html(h+":"+min+":"+s);
			});
		</script>
	</body>
</html>