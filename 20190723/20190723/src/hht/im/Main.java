package hht.im;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
//        try{
//            Socket socket = new Socket("www.google.com",80);//Socket(IP, port),主要當client端
//            System.out.println("本機位址:" + socket.getLocalAddress());
//            System.out.println("本機port:" + socket.getLocalPort());
//            System.out.println("遠端位址:" + socket.getInetAddress());
//            System.out.println("遠端port:" + socket.getPort());
//        }catch (IOException e){
//            System.out.println("error");
//        }
        try {
            //請求連線
            ServerSocket serversocket = new ServerSocket(8000);
            System.out.println("開始聆聽...");
            Socket socket = serversocket.accept();//接收到客戶連線
            System.out.println("已有客戶端連線");


            DataInputStream instream = new DataInputStream(socket.getInputStream());
            DataOutputStream outstream = new DataOutputStream(socket.getOutputStream());
            Scanner sc = new Scanner(System.in);
            while(true) {
                String getmsg = instream.readUTF();//收
                System.out.println("Client : " + getmsg);//收訊息
                if(getmsg.equals("stop"))break;
                //傳訊息
                String sendmsg = sc.next();
                outstream.writeUTF(sendmsg);//送
                System.out.println("Server : " + sendmsg);
                if(sendmsg.equals("stop"))break;
            }
            socket.close();
            serversocket.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
