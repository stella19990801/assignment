package hht.im;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class client {
    public static void main(String[] args) throws IOException {
        try {
            Socket socket = new Socket("127.0.0.1", 8000);
            DataInputStream instream = new DataInputStream(socket.getInputStream());
            DataOutputStream outstream = new DataOutputStream(socket.getOutputStream());
            Scanner sc = new Scanner(System.in);

            while(true) {
                //傳訊息
                String sendmsg = sc.next();
                outstream.writeUTF(sendmsg);//送
                System.out.println("Client : " + sendmsg);
                if(sendmsg.equals("stop"))break;

                String getmsg = instream.readUTF();//收
                System.out.println("Server : " + getmsg);//收訊息
                if(getmsg.equals("stop"))break;
            }
        }catch (UnknownHostException e){
            e.printStackTrace();
        }

    }
}
