package hht.im;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        // write your code here
        try {
            //請求連線
            ServerSocket serversocket = new ServerSocket(8001);
            System.out.println("開始聆聽...");
            Socket socket = serversocket.accept();//接收到客戶連線
            System.out.println("已有客戶端連線");
            ArrayList arr =new ArrayList();
            ArrayList ans =new ArrayList();
            ans.add(0,(int)(Math.random()*9+1));
            do{
                ans.add(1,((int)(Math.random()*10)));
            }while(ans.get(0)==ans.get(1));
            do{
                ans.add(2,((int)(Math.random()*10)));
            }while(ans.get(0)==ans.get(2)||ans.get(1)==ans.get(2));
            do{
                ans.add(3,((int)(Math.random()*10)));
            }while(ans.get(0)==ans.get(3)||ans.get(1)==ans.get(3)||ans.get(2)==ans.get(3));
//            System.out.println(ans);
            for(int i=0;i<4;i++)
                System.out.print(ans.get(i));
            DataInputStream instream = new DataInputStream(socket.getInputStream());
            DataOutputStream outstream = new DataOutputStream(socket.getOutputStream());
            Scanner sc = new Scanner(System.in);

            while(true) {
                int getmsg = Integer.parseInt(instream.readUTF());//收
                System.out.println("Client : " + getmsg);//收訊息
                arr.add(0,getmsg/1000);
                arr.add(1,getmsg/100%10);
                arr.add(2,getmsg/10%10);
                arr.add(3,getmsg%10);

                //傳訊息
                String sendmsg;
                int A=0,B=0;
                for(int i=0;i<4;i++)
                    for(int j =0;j<4;j++)
                        if(i==j&&arr.get(i)==ans.get(j))
                            A++;
                        else if(i!=j&&arr.get(i)==ans.get(j))
                            B++;
                sendmsg = A+"A"+B+"B";
                outstream.writeUTF(sendmsg);//送
                System.out.println("Server : " + sendmsg);
                if(sendmsg.equals("4A0B"))break;
            }
            socket.close();
            serversocket.close();
            arr.clear();
            ans.clear();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
