package hht.im;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class client {
    public static void main(String[] args) throws IOException {
        try {
            Socket socket = new Socket("127.0.0.1", 8001);
            DataInputStream instream = new DataInputStream(socket.getInputStream());
            DataOutputStream outstream = new DataOutputStream(socket.getOutputStream());
            Scanner sc = new Scanner(System.in);

            while(true) {
                //傳訊息
                String sendmsg;
                int a,b,c,d,S;
                do {
                    System.out.println("請輸入要猜的數字:");
                    sendmsg = sc.next();
                    S = Integer.parseInt(sendmsg);
                    a = S/1000;
                    b = S/100%10;
                    c = S/10%10;
                    d = S%10;
                    if(sendmsg.length()!=4)System.out.println("長度不符");
                    if(a==b||a==c||a==d||b==c||b==d||c==d)System.out.println("輸入的數字重複");
                }while(sendmsg.length()!=4||a==b||a==c||a==d||b==c||b==d||c==d);
                outstream.writeUTF(sendmsg);//送
                System.out.println("Client : " + sendmsg);

                String getmsg = instream.readUTF();//收
                System.out.println("Server : " + getmsg);//收訊息
                if(getmsg.equals("4A0B"))break;
            }
        }catch (UnknownHostException e){
            e.printStackTrace();
        }

    }
}
