package hht.im;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Main {
    static ServerSocket ss;
    static Socket socket;
    static DataOutputStream outstream;
    static DataInputStream instream;
    static Thread A = new T1();
//    static  boolean b =false;
    static class T1 extends Thread{
        public void run(){
            String getmsg = null;
            while(true) {
                try {
                    getmsg = instream.readUTF();
                    System.out.println("Client : " + getmsg);//收訊息
                    if(getmsg.equals("exit")){
                        A.interrupt();
                        socket.close();
                        ss.close();
                        System.exit(0);
                    }        //b=true;
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
	// write your code here
        try {
            //請求連線
            ss = new ServerSocket(8000);
            System.out.println("開始聆聽...");
            socket = ss.accept();//接收到客戶連線
            System.out.println("已有客戶端連線");


            instream = new DataInputStream(socket.getInputStream());
            outstream = new DataOutputStream(socket.getOutputStream());
            Scanner sc = new Scanner(System.in);
            A.start();
            while(true) {
                //傳訊息
                String sendmsg = sc.next();
                outstream.writeUTF(sendmsg);//送
                System.out.println("Server : " + sendmsg);
                if(sendmsg.equals("exit")){
                    A.interrupt();
                    socket.close();
                    ss.close();
                    System.exit(0);
                    break;
                }
            }
            socket.close();
            ss.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
