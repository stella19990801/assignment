<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Hello, world!</title>
	<style>
		.fo{
			color:white;
			background-color:gray;
			display:block;
			text-align:center;
		}
	</style>
</head>
  <body>

  <div class="container-fluid">
	<img src="opening.png" width="100px"/>
	  <div class="row">
		<div class="col-12 col-lg-12 col-md-12 col-sm-12" style="padding:0px;">
			<img src="com.png" width=100% height="100%"/>
		</div>
		<div class="col-12 col-lg-12 col-md-12 col-sm-12" style="padding:0px;">
			<img src="man2.png" width=100% height="100%"/>
		</div>
		<div class="col-6 col-lg-3 col-md-6 col-sm-6" style="padding:0px;">
			<img src="Swift7.png" width=100% height="90%"/>
			<p align="center">Swift7</p>
		</div>
		<div class="col-6 col-lg-3 col-md-6 col-sm-6" style="padding:0px;">
			<img src="Swift5.png" width=100% height="90%"/>
			<p align="center">Swift5</p>
		</div>
		<div class="col-6 col-lg-3 col-md-6 col-sm-6" style="padding:0px;">
			<img src="Nitro50.png" width=100% height="90%"/>
			<p align="center">Nitro 50</p>
		</div>
		<div class="col-6 col-lg-3 col-md-6 col-sm-6" style="padding:0px;">
			<img src="AspireS24.png" width=100% height="90%"/>
			<p align="center">Aspire S24</p>
		</div>
		<div class="col-12 col-lg-3 col-md-6 col-sm-12" style="padding:0px;">
			<img src="Master.png" width=100% height="100%"/>
		</div>
		<div class="col-12 col-lg-3 col-md-6 col-sm-12" style="padding:0px;">
			<img src="Acer.jpg" width=100% height="100%"/>
		</div>
		<div class="col-12 col-lg-3 col-md-6 col-sm-12" style="padding:0px;">
			<img src="man.png" width=100% height="100%"/>
		</div>
		<div class="col-12 col-lg-3 col-md-6 col-sm-12" style="padding:0px;">
			<img src="robot.png" width=100% height="100%"/>
		</div>
	  </div>
	</div>
	<footer class="fo">
		 <div class="container-fluid">
			  <div class="row">
				<div class="col-lg-3">
					關於ACER</br></br>
					聯絡我們</br>
					投資者關係</br>
					新聞</br>
					企業社會責任</br>
					招兵買馬</br>
					獎項</br>
				</div>
				<div class="col-lg-3 ">
					服務</br></br>
					ACER ID</br>
					驅動程式與說明手冊</br>
					疑難排解文章</br>
				</div>
				<div class="col-lg-3">
					資源</br></br>
					創新</br>
					產品註冊</br>
					PREDATOR</br>
					NEXT@ACER</br>
					ACER DESIGN</br>
					銷售地點</br>
					軟體</br>
					價格表</br></br></br>
				</div>
				<div class="col-lg-3">
					法律聲明</br></br>
					隱私權聲明</br>
					法律聲明</br>
					宏碁股份有限公司營業規章</br>
					其他法律資訊</br>
				</div>
				<div class="col-12 col-md-12 col-sm-12">
					<div class="accordion" id="accordionExample">
					  <div class="card">
						<div class="card-header" id="headingOne"style="color:white;display:block;background-color:gray">
						  <h2 class="mb-0">
							<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="text-decoration:none;color:white">
							  關於ACER
							</button>
						  </h2>
						</div>

						<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
						  <div class="card-body" style="color:white;display:block;background-color:gray">
							聯絡我們</br>
							投資者關係</br>
							新聞</br>
							企業社會責任</br>
							招兵買馬</br>
							獎項</br>
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header" style="color:white;display:block;background-color:gray" id="headingTwo">
						  <h2 class="mb-0">
							<button class="btn btn-link collapsed" style="text-decoration:none;color:white" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							  服務
							</button>
						  </h2>
						</div>
						<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
						  <div class="card-body" style="color:white;display:block;background-color:gray">
							ACER ID</br>
							驅動程式與說明手冊</br>
							疑難排解文章</br>
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header" style="color:white;display:block;background-color:gray" id="headingThree">
						  <h2 class="mb-0">
							<button class="btn btn-link collapsed"  style="text-decoration:none;color:white" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							  資源
							</button>
						  </h2>
						</div>
						<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
						  <div class="card-body" style="color:white;display:block;background-color:gray">
							創新</br>
							產品註冊</br>
							PREDATOR</br>
							NEXT@ACER</br>
							ACER DESIGN</br>
							銷售地點</br>
							軟體</br>
							價格表</br>
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header" style="color:white;display:block;background-color:gray" id="headingThree">
						  <h2 class="mb-0">
							<button class="btn btn-link collapsed" style="text-decoration:none;color:white" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							  法律聲明
							</button>
						  </h2>
						</div>
						<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
						  <div class="card-body" style="color:white;display:block;background-color:gray">
								隱私權聲明</br>
								法律聲明</br>
								宏碁股份有限公司營業規章</br>
								其他法律資訊</br>
						  </div>
						</div>
					  </div>
					</div>
				</div>
			</div>
	</footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>