package com.example.a20190729_1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button bt;
    TextView textv;
    EditText ed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt = findViewById(R.id.button);
        textv = findViewById(R.id.textView);
        ed = findViewById(R.id.edtx);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textv.setText(ed.getText().toString());
            }
        });
    }
}
