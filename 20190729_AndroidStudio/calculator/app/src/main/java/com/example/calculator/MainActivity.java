package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button bt0, bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9, add, minus, mul, div, equ, B, C;
    TextView txt, ans;
    String a = "";
    Integer i = 0, y = 0;
    double Ans = 0;
    Integer x [] = new Integer [100];
    String op [] = new String[100];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txt = findViewById(R.id.textv);
        ans = findViewById(R.id.ans);
        bt0 = findViewById(R.id.bt0);
        bt0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s="";
                s = txt.getText()+"0";
                a += "0";
                txt.setText(s);
            }
        });
        bt1 = findViewById(R.id.bt1);
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s="";
                s = txt.getText()+"1";
                a += "1";
                txt.setText(s);
            }
        });
        bt2 = findViewById(R.id.bt2);
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s="";
                s = txt.getText()+"2";
                a += "2";
                txt.setText(s);
            }
        });
        bt3 = findViewById(R.id.bt3);
        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s="";
                s = txt.getText()+"3";
                a += "3";
                txt.setText(s);
            }
        });
        bt4 = findViewById(R.id.bt4);
        bt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s="";
                s = txt.getText()+"4";
                a += "4";
                txt.setText(s);
            }
        });
        bt5 = findViewById(R.id.bt5);
        bt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s="";
                s = txt.getText()+"5";
                a += "5";
                txt.setText(s);
            }
        });
        bt6 = findViewById(R.id.bt6);
        bt6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s="";
                s = txt.getText()+"6";
                a += "6";
                txt.setText(s);
            }
        });
        bt7 = findViewById(R.id.bt7);
        bt7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s="";
                s = txt.getText()+"7";
                a += "7";
                txt.setText(s);
            }
        });
        bt8 = findViewById(R.id.bt8);
        bt8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s="";
                s = txt.getText()+"8";
                a += "8";
                txt.setText(s);
            }
        });
        bt9 = findViewById(R.id.bt9);
        bt9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s="";
                s = txt.getText()+"9";
                a += "9";
                txt.setText(s);
            }
        });
        add = findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s="";
                s = txt.getText()+add.getText().toString();
                txt.setText(s);
                op[i] = "+";
                y = Integer.valueOf(a);
                x[i] = y;
//                ans.setText(String.valueOf(x[i]));
                i++;
                a="";
            }
        });
        minus = findViewById(R.id.minus);
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s="";
                s = txt.getText()+"-";
                txt.setText(s);
                op[i] = "-";
                y = Integer.valueOf(a);
                x[i++] = y;
                a="";
            }
        });
        mul = findViewById(R.id.multipy);
        mul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s="";
                s = txt.getText()+"*";
                txt.setText(s);
                op[i] = "*";
                y = Integer.valueOf(a);
                x[i++] = y;
                a="";
            }
        });
        div = findViewById(R.id.divide);
        div.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s="";
                s = txt.getText()+"/";
                txt.setText(s);
                op[i] = "/";
                y = Integer.valueOf(a);
                x[i++] = y;
                a="";
            }
        });
        C = findViewById(R.id.btC);
        C.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt.setText("計算式 : ");
                for(int n = 0;n <= i; n++){
                    x[n]=0;
                    op[n]="";
                }
                i=0;
            }
        });
        equ = findViewById(R.id.eq);
        equ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                y = Integer.valueOf(a);
                x[i++] = y;
                Ans = x[0];
                String c ="";
                //ans.setText(String.valueOf(x[0]));
                for(int n =0;n<i;n++){
                    c = op[n];
                    if(n != i-1) {
                        switch (c) {
                            case "+":
                                Ans += x[n + 1];
                                break;
                            case "-":
                                Ans -= x[n + 1];
                                break;
                            case "*":
                                Ans *= x[n + 1];
                                break;
                            case "/":
                                Ans /= x[n + 1];
                                break;
                        }
                    }
                    //ans.setText(ans.getText()+String.valueOf(x[n]));
                }
                String s = "答案 : " + Ans;
                ans.setText(s);
            }
        });
    }
}
