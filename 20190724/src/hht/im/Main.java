//package hht.im;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    static ServerSocket ss;
    static Socket socket;
    static DataOutputStream outstream;
    static DataInputStream instream;
    static ArrayList<String> name = new ArrayList<>();
    static ArrayList<T1> t = new ArrayList<>();
    //    static  boolean b =false;
    static class T1 extends Thread{
        String n;
        Socket socket;
        DataInputStream instream;
        DataOutputStream outstream;
        T1(String name, Socket s) throws IOException {
            n = name;
            this.socket = s;
            outstream = new DataOutputStream(socket.getOutputStream());
            instream = new DataInputStream(socket.getInputStream());
        }
        public void run(){
            String getmsg = null;
                    try {
                        aaa :
                        {
                            while (true) {
                                getmsg = instream.readUTF();
                                //System.out.println(getmsg);
                                for (int i = 0; i < name.size(); i++) {
                                    if (getmsg.equals("exit")) {
                                        System.out.println(n + "離開聊天室");
                                        for (int j = 0; j < name.size(); j++)
                                            if(!name.get(j).equals(n))
                                                t.get(j).outstream.writeUTF(n + ":" + "離開聊天室");
                                        name.remove(i);
                                        t.remove(i);
                                        outstream.close();
                                        instream.close();
                                        socket.close();
                                        break aaa;
                                    }else if (name.get(i).equals(n))
                                        continue;
                                    else {
                                        //String getnm = name.get(i);
                                            t.get(i).outstream.writeUTF(n + ":" + getmsg);
                                    }
                                }
                            }
                        }
                    } catch (IOException e) {
                        System.out.println(4);
                        e.printStackTrace();
                    }
        }
    }

    public static void main(String[] args) {
        // write your code here
        try {
            //請求連線
            ss = new ServerSocket(9000);
            System.out.println("開始聆聽...");
            while(true) {
                socket = ss.accept();//接收到客戶連線
                System.out.println("已有客戶端連線");
                instream = new DataInputStream(socket.getInputStream());
                String n = instream.readUTF();
                System.out.println(n + "進入聊天室");//收訊息
                name.add(n);
                T1 x = new T1(n,socket);
                t.add(x);
                x.start();
                for(int i = 0; i < name.size();i++){
                    if(name.get(i).equals(n)){
                        t.get(i).outstream.writeUTF("你加入了聊天室");
                        continue;
                    }else {
                        t.get(i).outstream.writeUTF(n + "加入聊天室");
                    }
                }
            }
        }catch(IOException e){
            System.out.println(3);
            e.printStackTrace();
        }
    }
}
