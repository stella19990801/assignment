//package hht.im;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class client {
    //static boolean b = false;
    static Socket socket;
    static DataOutputStream outstream;
    static DataInputStream instream;
    static Thread A = new T1();
    static class T1 extends Thread{
        public void run(){
            String getmsg = null;String getnm = null;
            try {
                String msg;
                msg = instream.readUTF();
                System.out.println(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
            while(true) {
                try {
                    getmsg = instream.readUTF();
                    System.out.println(getmsg);//收訊息
                    if(getmsg.equals("exit")){
                        A.interrupt();
                        //socket.close();
                        System.exit(0);
                    }
                } catch (IOException e) {
                    //System.out.println(2);
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        // write your code here
        try {
            //請求連線
            socket = new Socket("127.0.0.1",9000);
            instream = new DataInputStream(socket.getInputStream());
            outstream = new DataOutputStream(socket.getOutputStream());
            Scanner sc = new Scanner(System.in);
            String name = sc.next();
            outstream.writeUTF(name);
            A.start();
            while(true) {
                //傳訊息
                String sendmsg = sc.next();
                outstream.writeUTF(sendmsg);//送
                //System.out.println("Client : " + sendmsg);
                if(sendmsg.equals("exit")){
                    System.out.println("你已離開聊天室");
                    A.interrupt();
                    outstream.close();
                    instream.close();
                    socket.close();
                    System.exit(0);
                    break;
                }
            }
        }catch(IOException e){
            System.out.println(1);
            e.printStackTrace();
        }
    }
}
