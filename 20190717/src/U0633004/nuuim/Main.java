package U0633004.nuuim;

import java.util.*;

public class Main {
    public static class bank implements Comparable<bank>, U0633004.nuuim.bank {
        private String name;
        private int balance;
        bank(String n, int m){
            name = n;
            balance = m;
        }
        @Override
        public String toString() {
            return String.format("(%s, %d)", name, balance);
        }
        @Override
        public int compareTo(bank o) {
            return this.balance - o.balance;
        }

    }
    public static class Account{
        protected double balance;
        Account(){
            balance = 0;
        }
        public double credit(double m){//存款
            balance += m;
            return balance;
        }
        public double debit(double m){//提款
            balance -= m;
            return balance;
        }
    }
    public static class SavingAccount extends Account{//儲蓄帳戶
        private double interestRate;
        public void setInterestRate(double r){
            interestRate = r;
        }
        public double getInterestRate(){
            return interestRate;
        }
        public void calculateInterest(){
            balance += (balance*interestRate);
            System.out.println("存款餘額: " + balance);
        }
    }
    public static class CheckAccount extends Account{//支票帳號
        public double transactionFee;
        public double credit(double m){
            chargeFee(m);
            balance += (m - transactionFee);
            return balance;
        }
        public double debit(double m){
            chargeFee(m);
            balance -= (m - transactionFee);
            return balance;
        }
        public void chargeFee(double m){
            transactionFee = m * 0.0005;
        }
    }
    public static abstract class Animal{
        String name;
        public Animal(){
            name = "animal";
        }
        public void setName(String n){
            name = n;
        }
        public abstract void move();
        public abstract void sound();
    }
    public static void main(String[] args) {
	// write your code here
//        Scanner sc = new Scanner(System.in);
//        SavingAccount A = new SavingAccount();
//        A.setInterestRate(0.005);
//        System.out.print("請A(儲蓄帳戶)輸入存款金額:");
//        double m = sc.nextDouble();
//        A.credit(m);
//        A.calculateInterest();
//        System.out.println("利息: " + A.getInterestRate());
//        System.out.print("\n請A(儲蓄帳戶)輸入提款金額:");
//        m = sc.nextDouble();
//        A.debit(m);
//        A.calculateInterest();
//        System.out.println("利息: " + A.getInterestRate());
//        CheckAccount B = new CheckAccount();
//        System.out.print("\n請B(支票帳號)輸入存款金額:");
//        m = sc.nextDouble();
//        System.out.println("存款餘額: " +  B.credit(m));
//        System.out.print("\n請B(支票帳號)輸入提款金額:");
//        m = sc.nextDouble();
//        System.out.println("存款餘額: " +  B.debit(m));
//        Animal o = new Animal(){
//            @Override
//            public void move() {
//                System.out.println("\nMove : walk");
//            }
//            @Override
//            public void sound() {
//                System.out.println("Sound : rrrrr");
//            }
//        };
//        o.move();
//        o.sound();
//        System.out.println(o.name);
///////////////////////////////////////////////////////////////
        List<bank> arr = Arrays.asList(
                new bank("Mary",30),
                new bank("Gary",78),
                new bank("Tery",66),
                new bank("Ben",1200),
                new bank("Alice",18)
        );
        Collections.sort(arr);//使用介面的方式
        System.out.println(arr);
        arr.sort(//沒有使用介面的做法, Lambda: 參數 -> 回傳值
                Comparator.<bank,String>comparing(p -> p.name)
        );
        System.out.println(arr);

    }
}