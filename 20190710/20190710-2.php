<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title></title>
		<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	</head>
	<body>
		<input type="text" value="新北市" id="ip"/>
		<select id="opt"> 
            <option value="County">縣市</option>
            <option value="SiteName">測站名稱</option>
		</select>

		<button type="button" id="search">Search</button>
		<div id="content">
		<div>
		<script type="text/javascript">
			$("#search").on('click',function(){
				$.ajax({
				  //指定要對哪個URL提出Ajax要求
				  url: "http://opendata.epa.gov.tw/webapi/Data/ATM00766/?$orderby=SiteId%20desc&$skip=0&$top=1000&format=json",
				  //指定要傳送的資料
				  type: "GET",
				  //指定傳回的資料類型，例如JOSN
				  dataType: "jsonp",
				  //指定當Ajax要求成功時所要執行的函式
				  success: function(result){
					  //$("#content").html(result);
					  //console.log(result);
					  var arr = result.filter(function(item,index){
						  if(($("#opt").val())=="County")
							return item.County == $("#ip").val();
						  if(($("#opt").val())=="SiteName")
							return item.SiteName == $("#ip").val();
					  });
					  var str = "";
					  var n=0;
					  for(item of arr){
						  str += "縣市: " + item.County + "</br>測站: " + item.SiteName + "</br>數值: " + item.Concentration +"</br></br>";
						  n++;
					  }
					  str += "資料比數: " + n + "筆";
					  $("#content").html(str);
				  }
				  //指定當Ajax要求失敗時所要執行的函式
				  //error: onError,
				  //指定當Ajax要求完成時所要執行的函式 (在成功或失敗函式執行完畢後)
				  //complete: onComplete
				});
				
			});
/*			document.getElementById("search").onclick = function(){
				var rqst ="";
				rqst = new XMLHttpRequest();
				rqst.onreadystatechange = function(){
					if (rqst.readyState==4 ){
						if(rqst.status==200){
							var resultStr = rqst.responseText;
							var resultObj = JSON.parse(resultStr);
							var str="";
							str = resultObj.find(function(item, index){
								return item.name == $("#nm").val();
							});
							if(str)
								$("#myDiv").html(str.grade);
							else
								$("#myDiv").html("none");
						}else{
							alert("錯誤碼 : "+rqst.status);
						}
					}
				};
				rqst.open("GET","grade.txt",true);
				rqst.send();
			};*/
		</script>
	</body>
</html>