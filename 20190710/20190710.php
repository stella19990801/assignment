<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title></title>
		<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	</head>
	<body>
		<input type="text" id="nm"/>
		<button type="button" id="request">Who am I</button>
		<div id="myDiv">
		<div>
		<script type="text/javascript">
			document.getElementById("request").onclick = function(){
				var rqst ="";
				rqst = new XMLHttpRequest();
				rqst.onreadystatechange = function(){
					if (rqst.readyState==4 ){
						if(rqst.status==200){
							var resultStr = rqst.responseText;
							var resultObj = JSON.parse(resultStr);
							var str="";
							str = resultObj.find(function(item, index){
								return item.name == $("#nm").val();
							});
							if(str)
								$("#myDiv").html(str.grade);
							else
								$("#myDiv").html("none");
						}else{
							alert("錯誤碼 : "+rqst.status);
						}
					}
				};
				rqst.open("GET","grade.txt",true);
				rqst.send();
			};
/*			var xmlhttp = new XMLHttpRequest();
			xmlhttp.open("GET","grade.txt",true);
			xmlhttp.send(); 
			xmlhttp.onreadystatechange=function(){
				if (xmlhttp.readyState==4 && xmlhttp.status==200){
					//console.log("123");
					//document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
					var resultObj = JSON.parse(xmlhttp.responseText);
					//var str = resultObj[0];
					//document.getElementById("myDiv").innerHTML="name:"+str.name+"</br>ID:"+str.ID+"</br>grade:"+str.grade;
					var str = resultObj.find(function(item, index){
						return item.name == $("#nm").val();
					});
					$("#myDiv").html(str.grade);
				}else{
					console.log(xmlhttp.status);
				}
			};
			*/
		</script>
	</body>
</html>