package U0633004.nuuim;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    public  static float tem(char type, float t){
        if(type=='c')//轉攝氏
            t = (t-32)*5/9;
        if(type=='f')
            t = t*9/5+32;
        return t;
    }
    public  static int A(int [] x, int [] y){
        int a = 0;
        for(int i = 0; i < 4; i++)
            if(x[i]==y[i])
                a++;
        return a;
    }
    public  static int B(int [] x, int [] y){
        int b = 0;
        for(int j = 0; j < 4; j++)
            for(int i = 0; i < 4; i++)
                if(i!=j&&(x[i]==y[j]))
                    b++;
        return b;
    }
    public static boolean SF(String [] T, String [] N){//桐花順
        boolean bt = false;
        boolean bn = false;
        for(int i = 1;i < 5; i++)
            if(T[i]==T[0])
                bt=true;

        return bt&&bn;
    }
    public static boolean S( String [] N){//順子
        boolean bn = false;
        int [] n = trans(N);

        //for(int i = 1;i < 5; i++)



        return bn;
    }
    public static int [] trans(String []N){
        int [] n = new int[5] ;
        for(int i=0;i<5;i++)
        switch (N[i]){
            case "A":
                n[i] = 1;
                break;
            case "T":
                n[i] = 10;
                break;
            case "J":
                n[i] = 11;
                break;
            case "Q":
                n[i] = 12;
                break;
            case "K":
                n[i] = 13;
                break;
            default:
                n[i] = Integer.parseInt(N[i]);
        }
        return n;
    }
    public static  String [] sort( String X []){
        String [] XT = new String [5];
        String [] XN = new String [5];
        for(int i = 0;i<5;i++) {
            XT[i]=X[i].substring(0,1);
            XN[i]=X[i].substring(1,2);
        }
        int [] Xn = trans(XN);
        String tmp;
        for(int i=0;i<5;i++)
            for(int j=0;j<5;j++)
                if(Xn[i]>Xn[j]){
                    tmp = X[i];
                    X[i]=X[j];
                    X[j]=tmp;
                }
        return X;
    }
    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        System.out.print("請輸入大於0的數字:");
        int n = sc.nextInt();
        while(n<0){
            System.out.println("輸入有誤");
            System.out.print("請輸入大於0的數字:");
            n = sc.nextInt();
        }
        for(int i = n - 1; i >= 0; i--) {
            for (int j = 0; j < n; j++) {
                if (i > j)
                    System.out.print(" ");
                else
                    System.out.print("*");
            }
            System.out.println();
        }
////////////////////////////////////////////////////////////
        int ans = (int)(Math.random()*100+1);
        System.out.println("答案是:"+ans);
        int myAns = 0;
        do{
            System.out.print("請輸入要猜的數字:");
            myAns = sc.nextInt();
            if(myAns > ans)
                System.out.println("猜太大!");
            else if(myAns < ans)
                System.out.println("猜太小!");
            else
                System.out.println("恭喜猜中!");
        }while(myAns != ans);
////////////////////////////////////////////////////////////
        int n3 = (int)(Math.random()*5+5);
        int arr [] = new int [n3];
        int arr1 [] = new int [n3];
        int n2 = arr.length - 1;
        for(int i = 0 ; i < arr.length;i++) {
            arr[i] = (int) (Math.random() * 100 + 1);
            System.out.print(arr[i] + "\t");
        }
        for(int i = 0;i < arr.length; i++){
            arr1[n2--] = arr[i];
        }
        System.out.println();
        for(int i = 0 ; i < arr.length;i++)
            System.out.print(arr1[i]+"\t");
////////////////////////////////////////////////////////////
        System.out.println("攝氏\t華氏");
        System.out.println("0度\t32度");
        System.out.println("20度\t68度");
        System.out.println("100度\t212度");
        float t=0;
        System.out.print("請輸入攝氏想轉華氏的溫度:");
        t = sc.nextFloat();
        System.out.println(tem('f',t));
        System.out.print("請輸入華氏想轉攝氏的溫度:");
        t = sc.nextFloat();
        System.out.println(tem('c',t));
////////////////////////////////////////////////////////////
        int [] A = new int [4];
        int [] B = new int [4];
        A[0] = (int)(Math.random()*9+1);
        do {
            A[1] = (int) (Math.random() * 10);
        }while(A[1]==A[0]);
        do{
           A[2] = (int)(Math.random()*10);
        }while(A[2]==A[0]||A[2]==A[1]);
        do{
            A[3] = (int)(Math.random()*10);
        }while(A[3]==A[0]||A[3]==A[1]||A[3]==A[2]);
        System.out.println("ANS:"+A[0]+A[1]+A[2]+A[3]);
         do {
            do {
                System.out.print("請輸入所猜的4個不同的數:");
                int g = sc.nextInt();
                B[0] = g / 1000;
                B[1] = g / 100 % 10;
                B[2] = g / 10 % 10;
                B[3] = g % 10;
            } while (B[1] == B[0] || B[2] == B[0] || B[2] == B[1] || B[3] == B[0] || B[3] == B[1] || B[3] == B[2]);
//            System.out.println("B:"+B[0]+B[1]+B[2]+B[3]);
            System.out.println(A(A,B)+"A"+B(A,B)+"B");
        }while(A(A,B)!=4);
        System.out.println("你贏了!");
////////////////////////////////////////////////////////////
//        //H3 ST SJ SK D2
//        String [] X = new String [5];
//        String [] Y = new String [5];
//        String [] XT = new String [5];
//        String [] YT = new String [5];
//        String [] XN = new String [5];
//        String [] YN = new String [5];
//        int [] Xn = new int [5];
//        int [] Yn = new int [5];
////        String [] num = {"A","2","3","4","5","6","7","8","9","T","J","Q","K"};
////        String [] type = {"S","H","D","C"};
//        System.out.print("請輸入第一副的5張牌:");
//        for(int i = 0;i<5;i++) {
//            X[i] = sc.next();
//            XT[i]=X[i].substring(0,1);
//            XN[i]=X[i].substring(1,2);
//        }
//        sort(X);
//        for(int i = 0;i<5;i++) {
//            XT[i]=X[i].substring(0,1);
//            XN[i]=X[i].substring(1,2);
//        }
//        Xn =trans(XN);
//        for(int i = 0;i<5;i++)
//            System.out.println(Xn[i]);
///*        System.out.print("請輸入第一副的5張牌:");
//        for(int i = 0;i<5;i++) {
//            Y[i] = sc.next();
//            YT[i]=X[i].substring(0,1);
//            YN[i]=X[i].substring(1,2);
//        }*/

    }
}
