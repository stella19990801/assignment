package hht.im;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
class Player{
    private int name;
    private int sumNum;
    private int x1,x2,y1,y2;
    public Player(int n) {
        name=n;//1 or 2  (玩家1 and 玩家2)
    }
    public Player() {
    }
    public void range(int x,int y) {//依現在下的位置來判斷可能獲勝的範圍
        x1=x-3;
        if(x1<0)x1=0;
        x2=x+3;
        if(x2>6)x2=6;
        y1=y-3;
        if(y1<0)y1=0;
        y2=y+3;
        if(y2>5)y2=5;
    }
    public boolean IsWin(int x,int y,int [][] checkerboard) {
        int times=0;
        range(x,y);
        for(int i=x1;i<=x2;i++) { //X軸
            if(checkerboard[y][i]==name)times++;else times=0;
            if(times==4){return true;}
        }
        times=0;
        for(int i=y1;i<=y2;i++) { //Y軸
            if(checkerboard[i][x]==name)times++;else times=0;
            if(times==4){return true;}
        }
        times=0;
        for(int i=y1,j=x1;i<=y2&&j<=x2;i++,j++) { //左下至右上
            if(checkerboard[i][j]==name)times++;else times=0;
            if(times==4){
                return true;
            }
        }
        times=0;
        for(int i=y2,j=x1;i>=y1&&j<=x2;i--,j++) { //左上至右下
            if(checkerboard[i][j]==name)times++;else times=0;
            if(times==4){return true;}
        }
        return false;
    }
    public int ShowName(){//下棋時紀錄是誰下的棋，其值為1或2
        return name;
    }

    public int getSumNum() {//判斷平手
        return sumNum;
    }

    public void setSumNum(int sumNum) {//計算下棋數
        this.sumNum = sumNum;
    }
}
public class Main {
    static ServerSocket ss;
    static Socket socket;
    static DataOutputStream outstream;
    static DataInputStream instream;
    public static int [][] checkerboard=new int [6][7];//棋盤
    public static int pn1 = 0;
    public static int pn2 = 0;
    ////////////////////////////////////////////////////////重玩
    public static void restart() {
        for(int i=0;i<6;i++)
            for(int j=0;j<7;j++)
                checkerboard[i][j]=0;
        startgame();
    }
    ////////////////////////////////////////////////////////開始遊戲

    public static void startgame() {
        Scanner sc = new Scanner(System.in);
        Player p1 = new Player(1);
        Player p2 = new Player(2);
        int x,y = 0;
        System.out.println("玩家一 投入的硬幣為數字1，玩家二 投入的硬幣為數字2");
        while(true) {
            //玩家一
            do {
                do {
                    System.out.print("請玩家一開始選擇投入的列號 :");
                    x = (sc.nextInt()) - 1;
                }while(x>7||x<0);//列號防呆
                String sendmsg = String.valueOf(x);
                try {
                    outstream.writeUTF(sendmsg);//送
                    if(sendmsg.equals("exit")){
                        socket.close();
                        ss.close();
                        System.exit(0);
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                y = pieceDown(x, p1);
                //System.out.println(p1.getSumNum());
            }while(y==-1);//該列是否已滿
            showCheckerboard();
            if(p1.IsWin(x,y,checkerboard)) {
                System.out.println("玩家一獲勝");
                pn1++;
                break;
            }
            do {
                do {
                    System.out.print("玩家二投入的列號");
                    String getmsg = "";
                    try {
                        getmsg = instream.readUTF();//x
                        if(getmsg.equals("exit")){
                            socket.close();
                            ss.close();
                            System.exit(0);
                        }        //b=true;

                        x = Integer.parseInt(getmsg);
                        System.out.println((x + 1));//收訊息
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }while (x>7||x<0);
                y=pieceDown(x,p2);
                if(p1.getSumNum()==21||p2.getSumNum()==21){
                    System.out.println("平手");
                    break;
                }
            }while(y==-1);
            showCheckerboard();
            if(p2.IsWin(x,y,checkerboard)) {
                System.out.println("玩家二獲勝");
                pn2++;
                break;
            }
            //依下棋數來判斷是否平手
            if(p1.getSumNum()==21||p2.getSumNum()==21)break;
        }
        System.out.printf("玩家一 : %d 勝\n玩家二 : %d 勝\n",pn1,pn2);
    }
    public static int  pieceDown(int x, Player Z) {// return y 下棋
        //從最下層，由下往上開始找為空的位置，如還有空位則填入該玩家的name，否則回傳一表示該列已滿
        for(int i = 5; i >= 0; i--) {
            if (checkerboard[i][x] == 0) {
                checkerboard[i][x] = Z.ShowName();
                Z.setSumNum((Z.getSumNum())+1);
                return i;//還未下過棋的的位置
            }
        }
        System.out.println("位置滿了");
        return -1;//滿了
    }
    ////////////////////////////////////////////////////////顯示目前的棋盤
    public static void showCheckerboard() {
        for(int i=0;i<8;i++)System.out.print(i+" ");System.out.println();
        for(int i=0;i<6;i++) {
            System.out.print(i+1+" ");
            for(int j=0;j<7;j++)System.out.print(checkerboard[i][j]+" ");System.out.println();
        }
    }
    public static void main(String[] args) {
        // write your code here
        try {
            //請求連線
            ss = new ServerSocket(8000);
            System.out.println("開始聆聽...");
            socket = ss.accept();//接收到客戶連線
            System.out.println("已有客戶端連線");


            instream = new DataInputStream(socket.getInputStream());
            outstream = new DataOutputStream(socket.getOutputStream());
            Scanner sc = new Scanner(System.in);
            while(true) {
                //傳訊息
                startgame();
                ////////////////////////////////////////////////////////是否重玩
                while(true) {
                    System.out.print("Play Again? Yes:1 NO:2");
                    if(sc.nextInt()==1)restart();else{
                        try {
                            String sendmsg = "exit";
                            outstream.writeUTF(sendmsg);//送
                                socket.close();
                                ss.close();
                                System.exit(0);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
//            socket.close();
//            ss.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
